var app = angular.module("app", ["xeditable"]);

app.run(function(editableOptions) {
  editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default'
});

app.controller('nameCtrl', function($scope) {
  $scope.user = {
    name: 'Blaenk Minds'
  };  
});

app.controller('locationCtrl', function($scope) {
  $scope.user = {
    location: 'Berlin'
  };  
});

app.controller('aboutCtrl', function($scope) {
  $scope.user = {
    about: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing'
  };  
});




